.. _modules:


Scientific Development Modules
==============================

Please find the list of available modules catogorised by the coding skill levels:

Occasional Coder Modules
------------------------

Git version control (single user)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The basics of Git for version control: How git works, basic commands, configurations, etc, focusing on single-user workflows.
Understand how to integrate Git with Osiris and related applications for efficient code management.

Clean code
^^^^^^^^^^
Principles of writing clean, readable, and maintainable code. 
Explore best practices for coding style (PEP8 for Python, tidyverse for R), naming conventions, and code refactoring.


Documentation
^^^^^^^^^^^^^
Techniques for creating comprehensive and user-friendly documentation. 
Learn about different types of documentation and tools to generate them.


Regular Coder Modules
---------------------

Git branches
^^^^^^^^^^^^
Working with Git branches to manage feature development and code integration. 
Understand branching strategies and workflows to enhance project organisation.


Git as a collaboration tools
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
GitLab and GitHub: Utilize Git for collaborative development. 
Learn about the features of GitLab and GitHub, including issue tracking, 
merge requests, and project management.

Reporoducibility
^^^^^^^^^^^^^^^^
Virtual environments: Use virtual environments to ensure reproducible software builds and isolated development environments (anaconda).
Sandboxes: Implement sandboxing techniques to create isolated and controlled execution environments for testing and development (Docker).


Debugging
^^^^^^^^^
Tools and techniques for debugging software. 
Understand common debugging methods, use of debuggers, and how to troubleshoot and resolve issues.


Code review
^^^^^^^^^^^
Best practices for conducting code reviews. 
Learn how to provide constructive feedback, improve code quality, and foster a collaborative development environment.



Advanced Coder Modules
----------------------

Test suits and unit testing
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Developing and running test suites and unit tests. 
Learn about testing frameworks and the importance of automated testing in 
ensuring software reliability.


Continuous integration and continuous deployment (CI/CD)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Implementing CI/CD pipelines to automate the building, testing, and 
deployment of software. Understand tools and best practices for continuous
integration and deployment.


Documentation tools
^^^^^^^^^^^^^^^^^^^
Learn how to use Sphinx for creating high-quality documentation. 
Understand the process of generating documentation from source code and 
other materials.



Copyright and licensing
^^^^^^^^^^^^^^^^^^^^^^^
Overview of software copyright and licensing. 
Learn about different open-source licenses and how to choose the appropriate 
license for your project.


software packaging
^^^^^^^^^^^^^^^^^^
Packaging software in R, Python, C/Fortran, etc. 
Learn the tools and techniques for distributing software packages in 
various programming languages.



Argument handeling and user interface
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Techniques for handling command-line arguments and designing user interfaces. 
Understand how to create intuitive and user-friendly interfaces for your software.



Passionate Coder Modules
------------------------

Code paradimes
^^^^^^^^^^^^^^
Explore different programming paradigms such as procedural, 
object-oriented, and functional programming. Understand their 
applications and benefits.



Computer architecture
^^^^^^^^^^^^^^^^^^^^^^
Overview of computer architecture and its implications for software development. 
Understand hardware components, memory hierarchy, and performance considerations.
Understand CPU multithreading and performance.


Parallel computing
^^^^^^^^^^^^^^^^^^
Introduction to parallel computing concepts and techniques. 
Explore methods for parallelising code to improve performance on multi-core processors and clusters (Osiris).


Makefile
^^^^^^^^
Introduction to Makefiles for automating build processes. 
Learn how to write Makefiles to compile code, run tests, and manage 
project dependencies.

Optimisation
^^^^^^^^^^^^
Strategies for optimizing code for better performance and efficiency. 
Learn about profiling, bottleneck identification, and algorithmic improvements.


compiled code
^^^^^^^^^^^^^
Connecting C with Python (Pybind11). 
Learn how to interface compiled C code with Python using Pybind11. 
Understand the benefits of using compiled code for performance-critical applications.
