.. Use
   sphinx-build -b html docs/source public
   to generate the html for preview

Scientific Code Development, an IARC training course
====================================================

Introduction
------------
The purpose of this training program is to equip IARC scientists with 
essential skills in scientific software development and the management of 
open-source packages. This training is crucial for achieving the goals of 
Open Science Policies, specifically to create open-access and reproducible 
research.

The important role of scientific software and packages in advancing 
research is briefly discussed  :ref:`here <spd_importance>`.
We have categorised the :ref:`coding skills and requirements <coding_skills>` 
of IARC's scientific staff based on  how frequently they need coding to 
advance their research. Whether you need to aquire skills as an 
:ref:`occasional_coder`, a :ref:`regular_coder`, or aim to enhance your 
expertise as an :ref:`advanced_coder`, we have the perfect training course for you:

Training Courses:
-----------------

.. _courses:

1. Git Started: Your First Steps in Scientific code
---------------------------------------------------
Rquired skill level: :ref:`occasional_coder` or :ref:`regular_coder`

Dive into the world of scientific development with this beginner-friendly course. 
In this course you will Learn the basics of:

- version control using git for small projects.
- clean coding practices.
- How to use git with Osiris and its Applications (Rstudio, VScode, Jupyter Notebook, etc), and essential tools to kickstart your journey.
- How to use the SIT Gitlab platform.
- Virtual environments (in R or Python) for reproducablity.
- Minimum requirements of good documentation and how to do it (README, etc)


2. Code Crusaders: Intermediate Adventures in Open Source
---------------------------------------------------------
Rquired skill level: :ref:`occasional_coder` or :ref:`regular_coder`

Embark on a heroic quest to conquer intermediate challenges in scientific software development. 

- Virtual environments (conda) for reproducablity (Nextflow).
- Use Git for branched development and multi-feature implamentation (merge/pull, rebase, etc).
- Documentation for users and developers (docstrings, roxygen).

3. Debugging Dragons: Slaying Bugs and Writing Clean Code
---------------------------------------------------------
Rquired skill level: :ref:`regular_coder` or :ref:`advanced_coder`

Take on the dragons of debugging with this course dedicated to: 

- Learn advanced debugging techniques and clean code principles.
- Test suits and unit testing
- Setup continuous integration and autometised testing.

4. Packaging Paladins: Crafting and Documenting Your Code
---------------------------------------------------------
Rquired skill level: :ref:`advanced_coder` 

Become a packaging paladin and learn the art of creating and distributing software packages. 

- Master tools for packaging in various languages.
- Generate professional documentation using tools like Sphinx to ensure your code is accessible and user-friendly.
- setup continuous deployment.
- Understand how to create intuitive and user-friendly interfaces for your software.
- Learn about different open-source licenses and how to choose the appropriate license for your project.

.. 
   5. Optimization Ninjas: Advanced Techniques for Code Warriors
   -------------------------------------------------------------
   :ref:`frequent_coder` + :ref:`passionate_coder`
   Hone your skills to become an elite code warrior with advanced optimization techniques, parallel computing, and compiled code integration. This course will transform you into a debugging and performance-tuning ninja.
   - Learn about the computer's CPU and memory architecture and how to take advantage of it.
   - Learn about profiling, bottleneck identification, and algorithmic improvements.
   - Learn how to interface compiled C code with Python using Pybind11.


Content:
========

.. toctree::
   :maxdepth: 1
   
   

   coding_skills
   importance_of_SPD
   training_modules

Indices and tables
==================

* :ref:`search`

.. * :ref:`modindex`
   * :ref:`genindex`