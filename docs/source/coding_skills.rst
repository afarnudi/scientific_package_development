.. _coding_skills:

Programming skill levels
------------------------
The IARC community can be differentiated into the following categories
based on their coding frequency:

.. _occasional_coder:

Occasional Coder
^^^^^^^^^^^^^^^^
    **Coding Frequency:** Sporadic, less than once a week.

    **Skills:**

    - Basic understanding of programming concepts.
    - Familiar with one or maybe two programming languages.
    - Can write simple programs or scripts when needed.
    - Mainly use Osiris throught the application interface (Rstudio, Jupyter-Notebook, Visual Studio, etc)

    **Responsibilities:**

    - Write code as necessary for specific tasks or projects.
    - Engage in coding primarily for learning or minor tasks.
    - Seek help from more frequent coders when encountering challenges.

.. _regular_coder:

Regular Coder
^^^^^^^^^^^^^^^
    **Coding Frequency:** Regularly, about once or twice a week.

    **Skills:**

    - Moderate understanding of programming concepts and tools.
    - Proficient in one or two programming languages.
    - Can work on small to medium-sized tasks independently.

    **Responsibilities:**

    - Contribute to coding projects on a part-time basis.
    - Maintain or update existing codebases.
    - Collaborate with more frequent coders for guidance and code reviews.

.. _advanced_coder:

Advanced Coder
^^^^^^^^^^^^^^
    **Coding Frequency:** Several times a week, but not daily.

    **Skills:**

    - Solid understanding of programming principles.
    - Proficient in multiple programming languages and technologies.
    - Can handle medium to complex coding tasks independently.

    **Responsibilities:**

    - Regularly contribute to ongoing projects.
    - Participate in team coding activities and code reviews.
    - Continuously improve coding skills through practice and learning.

.. 
    Daily Coder
    ^^^^^^^^^^^
    **Coding Frequency:** Almost daily, around five to six days a
    week.
    **Skills:**
    - Strong grasp of software development lifecycle and best
    practices.
    - High proficiency in multiple programming languages and
    frameworks.
    - Capable of leading development on complex projects.
    **Responsibilities:**
    - Lead or heavily contribute to significant coding projects.
    - Mentor less frequent coders.
    - Ensure code quality and consistency through regular code
    reviews and testing.

.. 
    Full-Time Coder
    ^^^^^^^^^^^^^^^
    **Coding Frequency:** Every workday, often coding as the primary
    job function.
    **Skills:**
    - Expert knowledge of software development, architecture, and
    design patterns.
    - Extensive experience in various programming languages and
    technologies.
    - Ability to solve highly complex problems and architect
    large-scale systems.
    **Responsibilities:**
    - Full-time contribution to coding projects and initiatives.
    - Lead development teams and major projects.
    - Set coding standards and ensure adherence to best practices.
    - Provide mentorship and technical leadership within the
    organisation.

.. 
    .. _passionate_coder:
    Passionate Coder
    ^^^^^^^^^^^^^^^^
        **Coding Frequency:** Every day, including personal time outside
        of work hours.
        **Skills:**
        - Deep passion for coding and continuous learning.
        - Extensive expertise across multiple domains and technologies.
        - Often involved in open-source projects, community contributions, or personal projects.
        **Responsibilities:**
        - Drive innovation and contribute to the coding community.
        - Take on complex and pioneering projects.
        - Mentor and inspire others through knowledge sharing and collaboration.
        - Balance professional and personal coding projects, demonstrating a deep commitment to the craft.