==============================
Scientific Package Development
==============================

The purpose of this training program is to equip IARC scientists with 
essential skills in scientific software development and the management of 
open-source packages. This training is crucial for achieving the goals of 
Open Science Policies, specifically to create open-access and reproducible 
research.

Please follow this `link <https://scientific-package-development-afarnudi-b573ebdf09a4b78e127b714.gitlab.io/>`_ for more information.