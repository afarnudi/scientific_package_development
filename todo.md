## General
- Talk to IT about potential user registration on their GitLab server + Osiris. We will use the external server.
- Not use git with OneDrive.
- Do not store data and big binary files on git
- define multiple users with different access to the project.
- Prepare prerequisite documents for the the participants (as simple and easy as possible)
    #### Session 1
    - Make sure everybody has a laptop
    - Check their GitLab access
    - Check their Osiris access
    - required software
        - RStudio + VScode for interested users
    - Windows users must have:
        - GitBash
    #### Session 2
    - Make sure everybody has a laptop
    - Check their GitLab access
    - Check their Osiris access
    - Prepare a local survay for day 2 to see how many people are intersted to continue.

## Session 1 Git started

0. [What this day is about]
The topics will be covered in the following order:
1. [Sergey ~15min + 10min Q&A] 
Introduction to Git: Version control for projects. 
2. [Sergey + Ali 1 hour + ?15min Q&A]
    - Guide slides for this section
    - (Theoretical) Create a repository, fork (GitHub), clone, etc [not from the terminal]
    - [Sergey] Make the first commit with a README on the Git platform
    - [Sergey] Introduce the concept of remote and local repo
    - [Sergey (terminal) + Ali (RStudio + OSIRIS APP for ease of use) + Damien (Cyrculating)] Git config (user + email)
        - SSH or HTTPS, Token? [Damien checks on code.osiris.fr]
        - [Ali] print the cheat sheet
        - status
        - commits
        - push/pull
        - [Ali] restores (restore a deleted file)
        - log
        - .gitignore (don't put data but put information on how to instruct users to get data)
        - renaming (shows as a file deletion and file creation if ```git mv``` is not used).
    - How to use git with OSIRIS and its Apps
        - Rstudio
        - vscode [**try the plugin to make sure it works**]
        - Jupyter-Notebook
        ...
    - [sergey : terminal]link up repos on PC and OSIRIS for seamless development. 
3. [Sergey 15min] link up local repos with server (Create an empty repo on server and push existing project using ```git init``` and ```git push```)
4. [Ali 30min + simple example code] Clean coding (R style guide) commenting and variable name.
5. [Sergey] Minimum requirements of good documentation and how to do it.
    - README
-1. [What we learned + whats next]
## Sesssion 2 Code crusaiders

0. What is this day about
1.  virtual environments
    - [Damien 30min + hands-on] on a programming language level (Renv, venv (python), etc)
        - library paths
        - list of internal and external
    - [Sergey 30min hands-on (yml)] global virtual level (conda)
        - Intro to conda and bioconda
        - Control over R version.
        - Simple programme that prints a lib version.
        - control over including multiple softwares.
    - [Sergey 15-30min] Nextflow:
        - Introduction + simple example (mention the link between conda + git)

2. [1h15m + 20min (hands-on)]
    - [Ali] Use Git for branched development and multi-feature implamentation (merge/pull, rebase, etc).

3. [Ali 45min] Documentation for users and developers (docstrings, roxygen2).
4. [Ali 15min] Talk about Licensing 
    - What to use if you don't knoww any. (gplv3) [Sergey is going to check the best choice]
-1. [What we learned + whats next]